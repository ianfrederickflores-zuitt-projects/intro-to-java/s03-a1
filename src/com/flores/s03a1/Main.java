package com.flores.s03a1;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        int num = 1;
        BigInteger factorial = BigInteger.ONE;
        System.out.println("Input any positive integer:");
        try {
            num = input.nextInt();
            if (num <= 0) {
                throw new ArithmeticException("Negative integers and zero not valid.");
            }
            else {
                int i = 1;
                while (i <= num) {
                    factorial = factorial.multiply(BigInteger.valueOf(i));
                    ++i;
                }
            }
        } catch (ArithmeticException e) {
            System.out.println("Negative integers and zero are not valid.");
        } catch (Exception e) {
            System.out.println("Invalid input, try again.");
        }
        System.out.printf("The factorial of %d = %d", num, factorial);
    }
}
